import { useState } from "react";

import Header from "./Components/Header/Header";
import Cards from "./Components/Cards/Cards/Cards";
import './App.scss'



const App = () => {



    const getCard = () => {
        let cart = JSON.parse(localStorage.getItem('cart'));
        return cart
    }
    const [itemsInCart, setItemsInCart] = useState(localStorage.getItem('cart') ? getCard() : [])

    const getFavourite = () => {
        let favourite = JSON.parse(localStorage.getItem('favourite'));
        return favourite
    }

    const [itemsInFavourite, setItemsInFavourite] = useState(localStorage.getItem('favourite') ? getFavourite() : [])


    const onAddItem = (id) => {
        setItemsInCart((previous) => {


            const existingProduct = previous.find((product) => product.id === id)

            const itemsInCart = existingProduct ? [...previous.filter(product => !(product.id === id)), { id, quantity: existingProduct.quantity + 1 }]
                : [...previous, { id, quantity: 1 }]
            localStorage.setItem('cart', JSON.stringify(itemsInCart))
            return itemsInCart
        })
    }

    const onAddFavourite = (product) => {


        if (
            itemsInFavourite.find((favItem) => product.id === favItem.id)
        ) {

            setItemsInFavourite(itemsInFavourite.filter(({ id }) => id !== product.id))
            return
        }

        setItemsInFavourite([...itemsInFavourite, product])

    }
    localStorage.setItem('favourite', JSON.stringify(itemsInFavourite))

    return (
        <>
            <div className="container">
                <Header countCart={itemsInCart.length}
                    countFavourite={itemsInFavourite.length}
                    itemsInFavourite={itemsInFavourite}
                    itemsInCart={itemsInCart}
                />
                <div className="products-wrapper">

                    <Cards onAddItem={onAddItem} onAddFavourite={onAddFavourite} />
                </div>
            </div>

        </>

    )

};
export default App
















