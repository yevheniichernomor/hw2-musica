import { useEffect, useState } from "react";
import sendRequest from "../../../Helpers/sendRequest";
import Modal from "../../Modal/Modal";
import Card from "../Card/Card";
import PropTypes from 'prop-types';


const Cards = ({ onAddItem, onAddFavourite }) => {
    const [open, setOpen] = useState(false);
    const [product, setProduct] = useState([])
    const [selectedProductId, setSelectedProductId] = useState(null)

    useEffect(() => {
        sendRequest('./API/products.json')
            .then((data) => {
                setProduct(data)
            })
    }, [])

    const products = product.map((product) => (
        <Card
        
            key={product.id}
            item={product}
            onModalOpen={() => {
                setOpen(true);
                setSelectedProductId(product.id)
            }}
            onAddFavourite={() => {
                onAddFavourite(product)
            }}
        />
    ))


    const closeModal = () => {
        setOpen(false)
        setSelectedProductId(null)
    };

    return (
        <>
            {products}
            {open && (
                <Modal text='Please,confirm to add this item'
                    onCancel={closeModal}
                    onConfirm={() => {
                        onAddItem(selectedProductId)
                        closeModal()

                    }}
                />
            )}
        </>
    )

};

Cards.propTypes = {
    onAddItem: PropTypes.func,
    closeModal: PropTypes.func

};

export default Cards;









