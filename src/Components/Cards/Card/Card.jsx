import AddToList from "../../Buttons/AddToList/AddToList"
import PropTypes from 'prop-types';
import './Card.scss'


const Card = ({ item, onModalOpen, onAddFavourite }) => {
    const { name, price, art, img, color, id } = item;
    
    const favourite = Boolean(JSON.parse(localStorage.getItem('favourite'))?.find((favourite) => favourite.id === id))
    
  

    return (

        <>

            <div className="card-wrapper">
                <div className="img-wrapper">
                    <img className="image" src={img} alt={name} />
                </div>
                <div className="flex-wrapp">
                    <h1 className="device-name">{name}</h1>
                    <div className="svg-wrapp">

                        <svg className={favourite ? 'blue-svg' : 'grey-svg'} onClick={() => {
                            onAddFavourite()
                        }} xmlns="http://www.w3.org/2000/svg" data-name="Layer 2" width="32" height="32"
                            viewBox="0 0 32 32">
                            <path
                                d="M29.95 12.68A1 1 0 0 0 29 12h-9.26L17 2.77a1 1 0 0 0-1.91 0L12.26 12H3a1 1 0 0 0-.6 1.8l7.39 5.54-3.72 9.29a1 1 0 0 0 1.54 1.16L16 23.27l8.39 6.52a1 1 0 0 0 1.54-1.16l-3.72-9.29 7.39-5.54a1 1 0 0 0 .35-1.12Z" />
                        </svg>
                    </div>
                </div>
                <p className="art">Art: {art}</p>
                <p className="color">Color: {color}</p>
                <div className="price-wrapp">
                    <p className="price">$ {price}</p>
                    <AddToList text='Add to List' onClick={() => {
                        onModalOpen()
                    }}

                    />
                </div>
            </div>

        </>
    )
};


Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    art: PropTypes.number,
    color: PropTypes.string,
    colorChange: PropTypes.func,
    onAddFavourite: PropTypes.func

};

export default Card


