
import './Modal.scss'
import ConfirmButton from '../Buttons/ConfirmButton/ConfirmButton';

const Modal = (props) => {

    const { text, onCancel, onConfirm } = props;



    return (
        <div className="total-wrapper" onClick={onCancel}>
            <div className='modal-wrapper' onClick={(e) => {
                e.stopPropagation()
            }}>

                <div className="main-wrapper">
                    {text}

                </div>
                <div className="btn-wrapper">
                    <ConfirmButton text='ok' onClick={onConfirm} />
                    <ConfirmButton text='cancel' onClick={onCancel} />
                </div>
            </div>
        </div>

    )
}
export default Modal





